export default {
  items: [
    {
      name: 'BuyBack',
      icon: 'icon-buyback',
      children: [
        {
          name: 'Moon Ore',
          icon: 'icon-moon-ore',
          url: '/moon-ore-buyback'
        },
        {
          name: 'Standard Ore',
          icon: 'icon-standard-ore'
        },
        {
          name: 'Planetary Interaction',
          icon: 'icon-planetary-interaction'
        },
        {
          name: 'Salvage',
          icon: 'icon-salvage'
        }
      ]
    },
    {
      name: 'Corporation',
      icon: 'icon-corporation',
      children: [
        {
          name: 'Overview',
          icon: 'icon-overview'
        },
        {
          name: 'API Checker',
          icon: 'icon-api-checker'
        },
        {
          name: 'Structure Fuel',
          icon: 'icon-structure-fuel'
        }
      ]
    },
    {
      name: 'Industry',
      icon: 'icon-industry',
      children: [
        {
          name: 'Reaction Planner',
          icon: 'icon-reaction-planner'
        },
        {
          name: 'Production Calculator',
          icon: 'icon-production-planner'
        },
        {
          name: 'SP Farming Calculator',
          icon: 'icon-sp-farming'
        }
      ]
    }
  ]
}
