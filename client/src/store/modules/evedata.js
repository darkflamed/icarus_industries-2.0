import Vue from 'vue'
import axios from '../../helpers/axios'
/*
|--------------------------------------------------------------------------
| Mutation Types
|--------------------------------------------------------------------------
*/
export const INIT_CHARACTER = 'INIT_CHARACTER'
export const INSERT_CHARACTER = 'INSERT_CHARACTER'
export const INSERT_CHARACTER_LIST = 'INSERT_CHARACTER_LIST'
export const INIT_CORPORATION = 'INIT_CORPORATION'
export const INSERT_CORPORATION = 'INSERT_CORPORATION'
export const INSERT_CORPORATION_LIST = 'INSERT_CORPORATION_LIST'
export const INIT_ALLIANCE = 'INIT_ALLIANCE'
export const INSERT_ALLIANCE = 'INSERT_ALLIANCE'
export const INSERT_ALLIANCE_LIST = 'INSERT_ALLIANCE_LIST'
export const INIT_OBSERVER = 'INIT_OBSERVER'
export const INSERT_OBSERVER = 'INSERT_OBSERVER'
export const INSERT_OBSERVER_LIST = 'INSERT_OBSERVER_LIST'
export const INIT_TYPE = 'INIT_TYPE'
export const INSERT_TYPE = 'INSERT_TYPE'
export const INSERT_TYPE_LIST = 'INSERT_TYPE_LIST'
/*
|--------------------------------------------------------------------------
| Initial State
|--------------------------------------------------------------------------
*/
const initialState = {
  account_characters: [],
  characters: {},
  corporations: {},
  alliances: {},
  types: {},
  observers: {}
}

/*
|--------------------------------------------------------------------------
| Mutations
|--------------------------------------------------------------------------
*/
const mutations = {
  [INIT_CHARACTER] (state, payload) {
    if (state.characters[payload.data] === undefined) {
      Vue.set(state.characters, payload.data, {
        _id: null,
        corporation_id: null,
        birthday: null,
        name: null,
        gender: null,
        race_id: null,
        bloodline_id: null,
        description: null,
        alliance_id: null,
        ancestry_id: null,
        security_status: null,
        portrait: {
          px64x64: null,
          px128x128: null,
          px256x256: null,
          px512x512: null
        }
      })
    }
  },
  [INSERT_CHARACTER] (state, payload) {
    Vue.set(state.characters, payload.character._id, payload.character)
  },
  [INSERT_CHARACTER_LIST] (state, payload) {
    for (var key in payload.characters) {
      if (!payload.characters.hasOwnProperty(key)) continue
      Vue.set(state.characters, payload.characters[key]._id, payload.characters[key])
    }
  },
  [INIT_CORPORATION] (state, payload) {
    if (state.corporations[payload.data] === undefined) {
      Vue.set(state.corporations, payload.data, {
        _id: null,
        corporation_name: null,
        ticker: null,
        member_count: null,
        ceo_id: null,
        corporation_description: null,
        tax_rate: null,
        creator_id: null,
        url: null,
        alliance_id: null,
        creation_date: null,
        portrait: {
          px64x64: null,
          px128x128: null,
          px256x256: null,
          px512x512: null
        }
      })
    }
  },
  [INSERT_CORPORATION] (state, payload) {
    Vue.set(state.corporations, payload.corporation._id, payload.corporation)
  },
  [INSERT_CORPORATION_LIST] (state, payload) {
    for (var key in payload.corporations) {
      if (!payload.corporations.hasOwnProperty(key)) continue
      Vue.set(state.corporations, payload.corporations[key]._id, payload.corporations[key])
    }
  },
  [INIT_ALLIANCE] (state, payload) {
    if (state.alliances[payload.data] === undefined) {
      Vue.set(state.alliances, payload.data, {
        _id: null,
        creator_corporation_id: null,
        creator_id: null,
        date_founded: null,
        executor_corporation_id: null,
        faction_id: null,
        name: null,
        ticker: null,
        portrait: {
          px64x64: null,
          px128x128: null,
          px256x256: null,
          px512x512: null
        }
      })
    }
  },
  [INSERT_ALLIANCE] (state, payload) {
    Vue.set(state.alliances, payload.alliance._id, payload.alliance)
  },
  [INSERT_ALLIANCE_LIST] (state, payload) {
    for (var key in payload.alliances) {
      if (!payload.alliances.hasOwnProperty(key)) continue
      Vue.set(state.alliances, payload.alliances[key]._id, payload.alliances[key])
    }
  },
  [INIT_OBSERVER] (state, payload) {
    if (state.observers[payload.data] === undefined) {
      Vue.set(state.observers, payload.data, {
        _id: null,
        corporation_id: null,
        name: null,
        solar_system_id: null,
        type_id: null,
        position: null
      })
    }
  },
  [INSERT_OBSERVER] (state, payload) {
    Vue.set(state.observers, payload.observer._id, payload.observer)
  },
  [INSERT_OBSERVER_LIST] (state, payload) {
    for (var key in payload.observers) {
      if (!payload.observers.hasOwnProperty(key)) continue
      Vue.set(state.observers, payload.observers[key]._id, payload.observers[key])
    }
  },
  [INIT_TYPE] (state, payload) {
    if (state.types[payload.data] === undefined) {
      Vue.set(state.types, payload.data, {
        _id: null,
        groupID: null,
        name: {
          en: ''
        }
      })
    }
  },
  [INSERT_TYPE] (state, payload) {
    Vue.set(state.types, payload.type._id, payload.type)
  },
  [INSERT_TYPE_LIST] (state, payload) {
    for (var key in payload.types) {
      if (!payload.types.hasOwnProperty(key)) continue
      Vue.set(state.types, payload.types[key]._id, payload.types[key])
    }
  }
}

/*
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
*/
const actions = {
  updateCharacter: (context, data) => {
    if (context.state.characters[data] === undefined) {
      context.commit(INIT_CHARACTER, {data})
      return axios.get('/character/' + data)
        .then((res) => {
          context.commit(INSERT_CHARACTER, {character: res.data})
          return true
        })
    } else {
      return false
    }
  },
  updateCharacters: (context, data) => {
    for (let key in data) {
      context.commit(INIT_CHARACTER, {data: data[key]})
    }
    return axios.post('/characters', {characterList: data})
      .then((res) => {
        context.commit(INSERT_CHARACTER_LIST, {characters: res.data})
        let corporations = []
        let alliances = []
        for (var key in res.data) {
          if (!res.data.hasOwnProperty(key)) continue
          if (corporations.indexOf(res.data[key].corporation_id) === -1 && !context.state.corporations[res.data[key].corporation_id]) corporations.push(res.data[key].corporation_id)
          if (alliances.indexOf(res.data[key].alliance_id) === -1 && !context.state.alliances[res.data[key].alliance_id]) alliances.push(res.data[key].alliance_id)
        }
        context.dispatch('updateCorporations', corporations)
        context.dispatch('updateAlliances', alliances)
        return true
      })
  },
  updateCorporation: (context, data) => {
    if (context.state.corporations[data] === undefined) {
      context.commit(INIT_CORPORATION, {data})
      return axios.get('/corporation/' + data)
        .then((res) => {
          context.commit(INSERT_CORPORATION, {corporation: res.data})
          if (res.data.alliance_id > 0 && !context.state.alliances[res.data.alliance_id]) {
            return context.dispatch('updateAlliance', res.data.alliance_id).then(res => {
              return true
            })
          } else {
            return true
          }
        })
    } else {
      return false
    }
  },
  updateCorporations: (context, data) => {
    for (let key in data) {
      context.commit(INIT_CORPORATION, {data: data[key]})
    }
    return axios.post('/corporations', {corporationList: data})
      .then((res) => {
        context.commit(INSERT_CORPORATION_LIST, {corporations: res.data})
        return true
      })
  },
  updateAlliance: (context, data) => {
    if (context.state.alliances[data] === undefined) {
      context.commit(INIT_ALLIANCE, {data})
      return axios.get('/alliance/' + data)
        .then((res) => {
          context.commit(INSERT_ALLIANCE, {alliance: res.data})
          return true
        })
    } else if (!context.state.alliances[data]._id) {
      setTimeout(this.a.actions.updateAlliance(context, data), 100)
    } else {
      return false
    }
  },
  updateAlliances: (context, data) => {
    for (let key in data) {
      context.commit(INIT_ALLIANCE, {data: data[key]})
    }
    return axios.post('/alliances', {allianceList: data})
      .then((res) => {
        context.commit(INSERT_ALLIANCE_LIST, {alliances: res.data})
        return true
      })
  },
  updateObservers: (context, data) => {
    for (let key in data) {
      context.commit(INIT_OBSERVER, {data: data[key]})
    }
    return axios.post('/observers', {observerList: data})
      .then((res) => {
        context.commit(INSERT_OBSERVER_LIST, {observers: res.data})
        return true
      })
  },
  updateTypes: (context, data) => {
    for (let key in data) {
      context.commit(INIT_TYPE, {data: data[key]})
    }
    return axios.post('/types', {typeList: data})
      .then((res) => {
        context.commit(INSERT_TYPE_LIST, {types: res.data})
        return true
      })
  }
}

/*
|--------------------------------------------------------------------------
| Getters
|--------------------------------------------------------------------------
*/
const getters = {
  getCharacter: state => characterId => state.characters[characterId],
  getCharacterCorporation: state => characterId => state.corporations[state.characters[characterId].corporation_id],
  getCharacterAlliance: state => characterId => state.alliances[state.characters[characterId].alliance_id],
  getCorporation: state => corporationId => state.corporations[corporationId],
  getAlliance: state => allianceId => state.alliances[allianceId],
  getObserver: state => observerId => state.observers[observerId],
  getType: state => typeId => state.types[typeId]
}

/*
|--------------------------------------------------------------------------
| Export the module
|--------------------------------------------------------------------------
*/
export default {
  state: initialState,
  mutations,
  actions,
  getters
}
