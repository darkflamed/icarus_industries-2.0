/*
|--------------------------------------------------------------------------
| Mutation Types
|--------------------------------------------------------------------------
*/
export const SET_USER_DATA = 'SET_USER_DATA'
export const UNSET_USER = 'UNSET_USER'
export const SET_USER_ID = 'SET_USER_ID'

/*
|--------------------------------------------------------------------------
| Initial State
|--------------------------------------------------------------------------
*/
const initialState = {
  email: null,
  characters: []
}

/*
|--------------------------------------------------------------------------
| Mutations
|--------------------------------------------------------------------------
*/
const mutations = {
  [SET_USER_ID] (state, payload) {
    state.id = payload.userId
  },
  [SET_USER_DATA] (state, payload) {
    state.email = payload.user.email
    if (payload.user.characters) {
      for (var key in payload.user.characters) {
        if (!payload.user.characters.hasOwnProperty(key)) continue
        state.characters.push(payload.user.characters[key].character_id)
      }
    }
  },
  [UNSET_USER] (state, payload) {
    state.id = null
    state.name = null
    state.email = null
  }
}

/*
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
*/
const actions = {
  setUserID: (context, user) => {
    context.commit(SET_USER_ID, {user})
  },
  setUserData: (context, user) => {
    context.commit(SET_USER_DATA, {user})
  },
  unsetAuthUser: (context) => {
    context.commit(UNSET_USER)
  }
}

/*
|--------------------------------------------------------------------------
| Getters
|--------------------------------------------------------------------------
*/
const getters = {
  isLoggedIn: (state) => {
    return !!(state.email)
  }
}

/*
|--------------------------------------------------------------------------
| Export the module
|--------------------------------------------------------------------------
*/
export default {
  state: initialState,
  mutations,
  actions,
  getters
}
