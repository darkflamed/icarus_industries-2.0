import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import evedata from './modules/evedata'
// import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

/* const vuexLocal = new VuexPersistence({
  storage: window.localStorage
}) */

export default new Vuex.Store({
  modules: {
    auth: auth,
    evedata: evedata
  },
  // plugins: [vuexLocal.plugin],
  strict: true
})
