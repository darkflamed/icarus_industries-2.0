import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import routes from './routes'
import store from '../store/index'
import tokens from '../helpers/tokens'

Vue.use(VueRouter)
axios.interceptors.request.use(config => {
  if (tokens.getAccessToken()) {
    config.headers['Authorization'] = 'Bearer ' + tokens.getAccessToken()
    config.headers['Content-Type'] = 'application/json'
    config.headers['Accept'] = 'application/json'
  }
  return config
}, error => {
  return Promise.reject(error)
})

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes
})

router.beforeEach(async (to, from, next) => {
  if (tokens.isLoggedIn() && !store.getters.isLoggedIn) {
    let {data: userData} = await axios.get(process.env.API_URL + '/user').catch(error => {
      if (error.response.status === 401 && error.response.data === 'Unauthorized') {
        /** Log out for now until I do the fucking refresh tokens */
        store.dispatch('unsetAuthUser')
          .then(() => {
            tokens.removeTokens()
            router.push({name: 'Login'})
          })
      }
    })
    if (userData) await store.dispatch('setUserData', userData)
  }
  if (to.meta.requiresAuth) {
    if (store.getters.isLoggedIn || tokens.isLoggedIn()) { return next() } else { return next({name: 'Login'}) }
  }
  if (to.meta.requiresGuest) {
    if (store.getters.isLoggedIn || tokens.isLoggedIn()) { return next({name: 'Home'}) } else { return next() }
  }
  next()
})

export default router
