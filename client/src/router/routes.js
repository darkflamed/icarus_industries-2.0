/** Containers */
import Full from '@/containers/Full'

/** Views */
import Dashboard from '@/views/Dashboard'
import Login from '@/views/auth/Login'
import Register from '@/views/auth/Register'
import MoonOre from '@/views/buyback/moon_ore/MoonOre'
import MoonOreOverview from '@/views/buyback/moon_ore/CharacterOverview'
import MoonOreCorporationOverview from '@/views/buyback/moon_ore/CorporationOverview'
import MoonOreCorporationReports from '@/views/buyback/moon_ore/CorporationReports'

export default [
  {
    path: '/',
    component: Full,
    children: [
      {
        path: '',
        name: 'Home',
        component: Dashboard
      },
      {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: { requiresGuest: true }
      },
      {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: { requiresGuest: true }
      },
      {
        path: '/moon-ore-buyback',
        component: MoonOre,
        meta: {requiresAuth: true},
        children: [
          {
            path: '',
            name: 'MoonOreBuyback',
            component: MoonOreOverview,
            meta: {requiresAuth: true}
          },
          {
            path: 'corporation-overview',
            name: 'MoonOreBuybackCorporationOverview',
            component: MoonOreCorporationOverview,
            meta: {requiresAuth: true}
          },
          {
            path: 'corporation-reports',
            name: 'MoonOreBuybackCorporationReports',
            component: MoonOreCorporationReports,
            meta: {requiresAuth: true}
          }
        ]
      }
    ]
  }
]
