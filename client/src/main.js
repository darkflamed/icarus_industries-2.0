// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import axios from './helpers/axios'
import Vuelidate from 'vuelidate'
import App from './App'
import router from './router'
import store from './store/index'

Vue.use(BootstrapVue)
Vue.use(Vuelidate)

/*
 * Axios/JWT
 */
Vue.prototype.$http = axios

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
