import axios from 'axios'
import tokens from './tokens'
import store from '../store/index'

var httpAxios = axios.create({
  baseURL: process.env.API_URL
})

httpAxios.interceptors.request.use(config => {
  if (tokens.getAccessToken()) {
    config.headers['Authorization'] = 'Bearer ' + tokens.getAccessToken()
    config.headers['Content-Type'] = 'application/json'
    config.headers['Accept'] = 'application/json'
  }
  return config
}, error => {
  return Promise.reject(error)
})

httpAxios.interceptors.response.use(response => {
  return response
}, error => {
  if (error.response.status === 401 && error.response.data === 'Unauthorized') {
    /** Log out for now until I do the fucking refresh tokens */
    store.dispatch('unsetAuthUser')
      .then(() => {
        tokens.removeTokens()
        // router.push({name: 'Login'})
      })

    /* return axios.post('/auth/refresh')
      .then(({data}) => {
        jwtToken.setToken(data.access_token);
        return axios(error.config);
      }); */
  } else if (error.response.status === 500) {
    // Vue.noty.error('Server Error')
    /* store.dispatch('unsetAuthUser')
    .then(() => {
      jwtToken.removeToken();
      router.push({name: 'login'});
    }); */
  }

  return Promise.reject(error)
})

export default httpAxios
