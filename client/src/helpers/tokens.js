export default {
  setAccessToken (accessToken) {
    window.localStorage.setItem('accessToken', accessToken)
  },
  setRefreshToken (refreshToken) {
    window.localStorage.setItem('refreshToken', refreshToken)
  },
  getAccessToken () {
    return window.localStorage.getItem('accessToken')
  },
  getRefreshToken () {
    return window.localStorage.getItem('refreshToken')
  },
  isLoggedIn () {
    return (!!(window.localStorage.getItem('accessToken') && !!(window.localStorage.getItem('refreshToken'))))
  },
  removeTokens () {
    window.localStorage.removeItem('refreshToken')
    window.localStorage.removeItem('accessToken')
  }
}
