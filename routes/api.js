var express = require('express')
var passport = require('passport')
var router = express.Router()

const { check, validationResult } = require('express-validator/check')
const { matchedData, sanitize } = require('express-validator/filter')

/** Controllers */
var userController = require('../app/controllers/userController')
var esiController = require('../app/controllers/esiController')
var observerController = require('../app/controllers/observerController')
var marketController = require('../app/controllers/marketController')
var characterController = require('../app/controllers/characterController')
var corporationController = require('../app/controllers/corporationController')
var typeController = require('../app/controllers/typeController')
var allianceController = require('../app/controllers/allianceController')

/** Auth Routes */
router.post('/auth/attempt', [check('email').isEmail(), check('password').isLength({min: 2})], userController.auth_attempt)
router.post('/auth/refresh', [check('email').isEmail(), check('password').isLength({min: 8})], userController.auth_refresh)
router.post('/auth/register', userController.register)
router.get('/user', passport.authenticate('jwt', {session: false}), userController.details)

/** Public Eve Data */
router.get('/character/:id', characterController.character)
router.post('/characters', [check('characterList').isArray()], characterController.characterList)

router.get('/corporation/:id', corporationController.corporation)
router.post('/corporations', [check('corporationList').isArray()], corporationController.corporationList)

router.get('/alliance/:id', allianceController.alliance)
router.post('/alliances', [check('allianceList').isArray()], allianceController.allianceList)

router.get('/observer/:id', observerController.observer)
router.post('/observers', [check('observerList').isArray()], observerController.observerList)

router.get('/type/:id', typeController.type)
router.post('/types', [check('typeList').isArray()], typeController.typeList)

/** ESI Routes */
router.get('/esi/url', passport.authenticate('jwt', {session: false}), esiController.url)
router.post('/esi/verify', passport.authenticate('jwt', {session: false}), esiController.verify)

/** Market Routes */
router.post('/market/processed_price', [check('refine_rate').isDecimal(), check('market_price_percentage').isDecimal(), check('type_id').isNumeric()], marketController.refined_price)

/** Temporary route to get all of the mining/character/corporation data for the moon buy back page */
router.get('/observers/history', observerController.observerHistory)
router.get('/test', esiController.test)

module.exports = router
