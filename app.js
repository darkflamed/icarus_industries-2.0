require('dotenv').config()
var express = require('express')
var createError = require('http-errors')
var path = require('path')
var logger = require('morgan')
var bodyParser = require('body-parser')
var passport = require('passport')
var cors = require('cors')

require('./app/mongoose')
var User = require('mongoose').model('user')

var apiRouter = require('./routes/api')

var app = express()

// Passport Setup
var JwtStrategy = require('passport-jwt').Strategy
var ExtractJwt = require('passport-jwt').ExtractJwt

passport.use(new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.APP_KEY,
  ignoreExpiration: true
}, function (jwtPayload, done) {
  User.findById(jwtPayload.sub, function (err, user) {
    if (err) {
      return done(err, false)
    }
    if (user) return done(null, user)
    else return done(null, false)
  })
}))

app.use(logger('dev'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api', apiRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  if (req.path.split('/')[1] === 'api') next(createError(404))
  else return res.sendFile(path.join(__dirname, 'public/index.html'))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  let message = err.message
  let stack = req.app.get('env') === 'development' ? err.stack : {}

  // render the error page
  res.status(err.status || 500)
  res.json({message, stack})
})

module.exports = app
