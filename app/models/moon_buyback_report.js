const mongoose = require('mongoose')

const Schema = mongoose.Schema

const MoonBuybackReportSchema = new Schema({
  corporation_id: Number,
  start_date: Date,
  end_date: Date,
  ore_prices: Object
}, { timestamps: true })

/**
 * Methods
 */

MoonBuybackReportSchema.methods = {

}

mongoose.model('moon_buyback_report', MoonBuybackReportSchema)
