const mongoose = require('mongoose')

const Schema = mongoose.Schema

const ObserversSchema = new Schema({
  _id: Number,
  corporation_id: Number,
  name: String,
  solar_system_id: Number,
  type_id: Number,
  position: Object
}, { timestamps: true, strict: false })

/**
 * Methods
 */

ObserversSchema.methods = {

}

mongoose.model('observers', ObserversSchema)
