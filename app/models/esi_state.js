const mongoose = require('mongoose')

const Schema = mongoose.Schema

const EsiStateSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: [true, 'Cannot be blank.']
  },
  state: {
    type: String
  }
}, { timestamps: true })

/**
 * Methods
 */

EsiStateSchema.methods = {

}

mongoose.model('esi_state', EsiStateSchema)
