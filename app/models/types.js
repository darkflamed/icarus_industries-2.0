const mongoose = require('mongoose')

const Schema = mongoose.Schema

const TypeSchema = new Schema({
  _id: Number
}, { timestamps: false, strict: false })

/**
 * Methods
 */

TypeSchema.methods = {

}

mongoose.model('types', TypeSchema)
