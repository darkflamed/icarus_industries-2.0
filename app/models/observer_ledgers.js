const mongoose = require('mongoose')

const Schema = mongoose.Schema

const ObserverLedgersSchema = new Schema({
  observer_id: Number,
  observer_corporation_id: Number,
  character_id: Number,
  type_id: Number,
  quantity: Number,
  recorded_corporation_id: Number
}, { timestamps: true, strict: false, toJSON: { virtuals: true } })

/**
 * Methods
 */

ObserverLedgersSchema.virtual('type', {
  ref: 'types',
  localField: 'type_id',
  foreignField: '_id',
  justOne: true
})

mongoose.model('observer_ledgers', ObserverLedgersSchema)
