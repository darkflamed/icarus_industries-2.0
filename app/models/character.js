const mongoose = require('mongoose')

const Schema = mongoose.Schema

const CharacterSchema = new Schema({
  _id: Number,
  corporation_id: Number,
  birthday: Date,
  name: String,
  gender: String,
  race_id: Number,
  bloodline_id: Number,
  description: String,
  alliance_id: Number,
  ancestry_id: Number,
  security_status: Number,
  portrait: Object
}, { timestamps: true, strict: false })

/**
 * Methods
 */

CharacterSchema.methods = {

}

mongoose.model('character', CharacterSchema)
