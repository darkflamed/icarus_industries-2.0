const mongoose = require('mongoose')

const Schema = mongoose.Schema

const CorporationSchema = new Schema({
  _id: Number,
  corporation_name: String,
  ticker: String,
  member_count: Number,
  ceo_id: Number,
  corporation_description: String,
  tax_rate: Number,
  creator_id: Number,
  url: String,
  alliance_id: Number,
  creation_date: Date,
  portrait: Object
}, { timestamps: true, strict: false })

/**
 * Methods
 */

CorporationSchema.methods = {

}

mongoose.model('corporation', CorporationSchema)
