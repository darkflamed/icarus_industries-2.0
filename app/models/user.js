const mongoose = require('mongoose')

const Schema = mongoose.Schema

const UserSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, 'Cannot be blank.'],
    match: [/\S+@\S+\.\S+/, 'is invalid'],
    index: true
  },
  password: {
    type: String,
    required: true
  }
}, { timestamps: true })

/**
 * Methods
 */

mongoose.model('user', UserSchema)
