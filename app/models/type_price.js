const mongoose = require('mongoose')

const Schema = mongoose.Schema

const TypePriceSchema = new Schema({
  _id: Number,
  buy: Object,
  sell: Object
}, { timestamps: true, strict: false })

/**
 * Methods
 */

TypePriceSchema.methods = {

}

mongoose.model('type_price', TypePriceSchema)
