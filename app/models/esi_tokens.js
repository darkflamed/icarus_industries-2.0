const mongoose = require('mongoose')

const Schema = mongoose.Schema

const EsiTokensSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    required: [true, 'Cannot be blank.']
  },
  character_id: {
    type: Number
  },
  corporation_id: {
    type: Number
  },
  refresh_token: {
    type: String,
    required: true
  },
  roles: {
    type: [String]
  }
}, { timestamps: true })

/**
 * Methods
 */

EsiTokensSchema.methods = {

}

mongoose.model('esi_tokens', EsiTokensSchema)
