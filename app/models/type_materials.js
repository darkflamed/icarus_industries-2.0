const mongoose = require('mongoose')

const Schema = mongoose.Schema

const TypeMaterialsSchema = new Schema({
  materialTypeID: Number,
  quantity: Number,
  typeID: Number
}, { strict: false })

/**
 * Methods
 */

TypeMaterialsSchema.methods = {

}

mongoose.model('type_materials', TypeMaterialsSchema)
