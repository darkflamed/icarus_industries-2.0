const mongoose = require('mongoose')

const Schema = mongoose.Schema

const AllianceSchema = new Schema({
  _id: Number,
  creator_corporation_id: Number,
  creator_id: Number,
  date_founded: Date,
  executor_corporation_id: Number,
  faction_id: Number,
  name: String,
  ticker: String
}, { timestamps: true, strict: false })

/**
 * Methods
 */

AllianceSchema.methods = {

}

mongoose.model('alliance', AllianceSchema)
