const axios = require('axios')
const mongoose = require('mongoose')
const TypeMaterialModel = mongoose.model('type_materials')
const TypePriceModel = mongoose.model('type_price')
const TypesModel = mongoose.model('types')
const { validationResult } = require('express-validator/check')

exports.refined_price = async function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })

  const exclude = [18, 19, 20, 21, 22, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 11396, 17425, 17426, 17428, 17429, 17432, 17433, 17436, 17437, 17440, 17441, 17444, 17445, 17448, 17449, 17452, 17453, 17455, 17456, 17459, 17460, 17463, 17464, 17466, 17467, 17470, 17471, 17865, 17866, 17867, 17868, 17869, 17870, 46675, 46676, 46677, 46678, 46679, 46680, 46681, 46682, 46683, 46684, 46685, 46686, 46687, 46688, 46689]
  if (exclude.includes(req.body.type_id)) return res.json({ price: 0 })

  let materialData = {}

  const materials = await TypeMaterialModel.find({ typeID: parseInt(req.body.type_id) })
  if (!materials || materials.length === 0) return { price: 0 }

  let totalPrice = 0
  for (const material of materials) {
    let matData = (await TypesModel.findOne({ _id: parseInt(material.materialTypeID) })).toObject()
    let typePrice = await exports.priceFromFuzzworks(material.materialTypeID)
    totalPrice += (typePrice * Math.floor(material.quantity * req.body.refine_rate))

    matData.base_price = typePrice
    matData.price_quantity = material.quantity
    matData.refine_rate = req.body.refine_rate
    matData.final_price = (typePrice * Math.floor(material.quantity * req.body.refine_rate))
    materialData[material.materialTypeID] = matData
  }
  res.json({
    price: (totalPrice / 100) * req.body.market_price_percentage,
    type: await TypesModel.findOne({ _id: parseInt(req.body.type_id) }),
    materials: materialData
  })
}

exports.priceFromFuzzworks = async function (typeId) {
  let price = await TypePriceModel.findOne({ _id: parseInt(typeId) })
  if (price) return price.sell.min

  let fuzzworks = await axios.get('https://market.fuzzwork.co.uk/aggregates/?region=60003760&types=' + typeId)

  TypePriceModel.collection.insert({ _id: typeId, buy: fuzzworks.data[typeId].buy, sell: fuzzworks.data[typeId].sell })

  return fuzzworks.data[typeId].sell.min
}
