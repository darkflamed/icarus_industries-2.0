const esi = require('../esi')
const mongoose = require('mongoose')
const ObserverModel = mongoose.model('observers')
const ObserverLedgerModel = mongoose.model('observer_ledgers')
const { validationResult } = require('express-validator/check')

exports.observerHistory = async function (req, res) {
  let ledger = await ObserverLedgerModel.find({})

  return res.json(ledger)
}

exports.observer = async function (req, res) {
  let observer = await ObserverModel.findOne({ _id: req.params.id })
  if (!observer) {
    observer = await esi.updateObserver(req.params.id).catch(err => {
      console.log(err)
      return res.json({})
    })
  }

  return res.json(observer)
}

exports.observerList = async function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })

  let observers = await ObserverModel.find({ _id: { $in: req.body.observerList } })

  if (Object.values(observers).length !== req.body.observerList.length) {
    /* character = await esi.updateCharacter(req.params.id).catch(err => {
      console.log(err)
      return res.json({})
    }) */ /** TODO */
  }

  return res.json(observers)
}