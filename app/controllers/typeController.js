const mongoose = require('mongoose')
const TypeModel = mongoose.model('types')
const { validationResult } = require('express-validator/check')

exports.type = async function (req, res) {
  let type = await TypeModel.findOne({ _id: req.params.id })
  return res.json(type)
}

exports.typeList = async function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })

  let types = await TypeModel.find({ _id: { $in: req.body.typeList } })

  return res.json(types)
}
