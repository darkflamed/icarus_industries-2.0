const mongoose = require('mongoose')
const CorporationModel = mongoose.model('corporation')
const esi = require('../esi')
const { validationResult } = require('express-validator/check')

exports.corporation = async function (req, res) {
  let corporation = await CorporationModel.findOne({ _id: req.params.id })
  if (!corporation) {
    corporation = await esi.updateCorporation(req.params.id).catch(err => {
      console.log(err)
      return res.json({})
    })
  }

  return res.json(corporation)
}

exports.corporationList = async function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })

  let corporationCount = await CorporationModel.count({ _id: { $in: req.body.corporationList } })

  if (corporationCount !== req.body.corporationList.length) {
    for (let key in req.body.corporationList) {
      if (req.body.corporationList[key] === null) continue
      await esi.updateCorporationIfNotExists(req.body.corporationList[key]).catch(err => {
        console.log(err)
        return res.json({})
      })
    }
  }

  let corporation = await CorporationModel.find({ _id: { $in: req.body.corporationList } })

  return res.json(corporation)
}
