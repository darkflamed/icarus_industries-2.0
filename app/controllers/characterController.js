const mongoose = require('mongoose')
const CharacterModel = mongoose.model('character')
const esi = require('../esi')
const { validationResult } = require('express-validator/check')

exports.character = async function (req, res) {
  let character = await CharacterModel.findOne({ _id: req.params.id })
  if (!character) {
    character = await esi.updateCharacter(req.params.id).catch(err => {
      console.log(err)
      return res.json({error: 'Error updating character.'})
    })
  }

  return res.json(character)
}

exports.characterList = async function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })

  let characterCount = await CharacterModel.count({ _id: { $in: req.body.characterList } })

  if (characterCount !== req.body.characterList.length) {
    for (let key in req.body.characterList) {
      if (req.body.characterList[key] === null) continue
      await esi.updateCharacterIfNotExists(req.body.characterList[key]).catch(err => {
        console.log(err)
        return res.json({})
      })
    }
  }

  let character = await CharacterModel.find({ _id: { $in: req.body.characterList } })

  return res.json(character)
}
