const mongoose = require('mongoose')
const uuid = require('uuid/v1')
const _ = require('lodash')
const axios = require('axios')
const _ESI = require('eve-swagger').ESI
const esiWrapper = require('../esi')
const EsiStateModel = mongoose.model('esi_state')
const EsiTokensModel = mongoose.model('esi_tokens')

exports.test = function (req, res) {
  esiWrapper.updateAllMiningLogs()
    .then(result => res.json(result))
    .catch(err => console.error(err))
}

exports.url = function (req, res) {
  const scopes = [
    'esi-assets.read_assets.v1',
    'esi-assets.read_corporation_assets.v1',
    'esi-bookmarks.read_character_bookmarks.v1',
    'esi-bookmarks.read_corporation_bookmarks.v1',
    'esi-calendar.read_calendar_events.v1',
    'esi-calendar.respond_calendar_events.v1',
    'esi-characters.read_corporation_roles.v1',
    'esi-characterstats.read.v1',
    'esi-corporations.read_container_logs.v1',
    'esi-corporations.read_corporation_membership.v1',
    'esi-corporations.read_divisions.v1',
    'esi-corporations.read_facilities.v1',
    'esi-corporations.read_outposts.v1',
    'esi-corporations.read_starbases.v1',
    'esi-corporations.read_structures.v1',
    'esi-corporations.track_members.v1',
    'esi-corporations.write_structures.v1',
    'esi-fleets.read_fleet.v1',
    'esi-industry.read_character_mining.v1',
    'esi-industry.read_corporation_mining.v1',
    'esi-killmails.read_corporation_killmails.v1',
    'esi-killmails.read_killmails.v1',
    'esi-location.read_location.v1',
    'esi-location.read_online.v1',
    'esi-location.read_ship_type.v1',
    'esi-markets.structure_markets.v1',
    'esi-search.search_structures.v1',
    'esi-skills.read_skills.v1',
    'esi-universe.read_structures.v1',
    'esi-wallet.read_character_wallet.v1',
    'esi-wallet.read_corporation_wallets.v1'
  ]

  const state = uuid()

  let url = 'https://login.eveonline.com/oauth/authorize?response_type=code&redirect_uri=' +
          'http://localhost:8080/profile/verify-request-login' +
          '&client_id=83fc1d318dc8426489f3750ae26e6785'

  url += '&scope='
  _.forEach(scopes, scope => {
    url += scope
    url += ' '
  })
  url += `&state=${state}`

  EsiStateModel.findOneAndUpdate({
    user_id: req.user.id
  }, { state }, { upsert: true }, (err, doc) => {
    if (err) return res.status(500).json({ error: err })
    return res.json({ url })
  })
}

exports.verify = async function (req, res) {
  let config = {
    headers: {
      Authorization: `Basic ${Buffer.from(`${process.env.ESI_CLIENT_ID}:${process.env.ESI_SECRET}`).toString('base64')}`,
      'Content-Type': 'application/json'
    }
  }

  const data = {
    grant_type: 'authorization_code',
    code: req.body.code
  }

  const authData = await axios.post('https://login.eveonline.com/oauth/token', data, config)

  if (!authData.data.refresh_token) { return 'Refresh token not sent in response.' }

  config = {
    headers: {
      Authorization: `Bearer ${authData.data.access_token}`,
      'Content-Type': 'application/json'
    }
  }

  const charData = await axios.get('https://login.eveonline.com/oauth/verify', config)

  const ESI = new _ESI()
  const character = await ESI.characters(
    charData.data.CharacterID,
    authData.data.access_token
  ).details()
  const roles = await ESI.characters(
    charData.data.CharacterID,
    authData.data.access_token
  ).roles()

  esiWrapper.updateCharacter(charData.data.CharacterID)
  esiWrapper.updateCorporation(character.corporation_id)

  await EsiTokensModel.findOneAndUpdate({ user_id: req.user.id, character_id: charData.data.CharacterID }, {
    refresh_token: authData.data.refresh_token,
    corporation_id: character.corporation_id,
    roles
  }, { upsert: true })

  return res.json({
    authData: authData.data,
    charData: charData.data,
    character,
    roles
  })
}
