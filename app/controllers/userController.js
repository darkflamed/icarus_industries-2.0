const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const EsiTokenModel = mongoose.model('esi_tokens')
const UserModel = mongoose.model('user')
const { validationResult } = require('express-validator/check')

exports.auth_attempt = function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })
  UserModel.findOne({email: req.body.email}, (err, user) => {
    if (err) return res.status(500).json(err)
    if (!user) return res.status(401).json({error: 'Failed to log in.'})

    bcrypt.compare(req.body.password, user.password, (err, passed) => {
      if (err) return res.status(500).json(err)
      if (!passed) return res.status(401).json({error: 'Failed to log in.'})
      const token = jwt.sign({
        sub: user._id,
        exp: Math.floor((Date.now() / 1000) + (60 * 60))
      }, process.env.APP_KEY)
      res.json({ token })
    })
  })
}

exports.register = async function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })

  let count = await UserModel.count({ email: req.body.email })

  if (count > 0) return res.status(401).json({ error: { message: 'Email already exists' } })

  await UserModel.collection.insert({
    email: req.body.email,
    password: await bcrypt.hash(req.body.password, 10)
  }).catch(err => res.status(500).json({ error: { message: 'Error inserting user into database.' } }))

  res.json({ success: true })
}

exports.auth_refresh = function (req, res) {
  /** TODO */
}

exports.details = async (req, res) => {
  let characters = await EsiTokenModel.find({ user_id: req.user._id }, 'character_id')
  return res.json({ email: req.user.email, characters })
}
