const mongoose = require('mongoose')
const AllianceModel = mongoose.model('alliance')
const esi = require('../esi')
const { validationResult } = require('express-validator/check')

exports.alliance = async function (req, res) {
  let alliance = await AllianceModel.findOne({ _id: req.params.id })
  if (!alliance) {
    alliance = await esi.updateAlliance(req.params.id).catch(err => {
      console.log(err)
      return res.json({})
    })
  }

  return res.json(alliance)
}

exports.allianceList = async function (req, res) {
  if (!validationResult(req).isEmpty()) return res.status(422).json({ errors: validationResult(req).mapped() })

  let allianceCount = await AllianceModel.count({ _id: { $in: req.body.allianceList } })

  if (allianceCount !== req.body.allianceList.length) {
    for (let key in req.body.allianceList) {
      if (req.body.allianceList[key] === null) continue
      await esi.updateAllianceIfNotExists(req.body.allianceList[key]).catch(err => {
        console.log(err)
        return res.json({})
      })
    }
  }

  let alliance = await AllianceModel.find({ _id: { $in: req.body.allianceList } })

  return res.json(alliance)
}
