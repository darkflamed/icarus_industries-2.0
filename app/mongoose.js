var mongoose = require('mongoose')

const fs = require('fs')

fs
  .readdirSync(`${__dirname}/models`)
  .filter(file => (file.slice(-3) === '.js'))
  .forEach((file) => {
    require(`./models/${file}`)
  })

const URI = 'mongodb://localhost/icarus_industries'

mongoose.connect(URI)
mongoose.connection.on('connected', () => {
  console.info(`Mongoose default connection open to ${URI}`)
})

mongoose.connection.on('error', err => {
  console.error(`Mongoose default connection error: ${err}`)
})

mongoose.connection.on('disconnected', () => {
  console.info('Mongoose default connection disconnected')
})

process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    console.info('Mongoose default connection disconnected through app termination')
    process.exit(0)
  })
})
