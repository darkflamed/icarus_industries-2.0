require('dotenv').config()
const fs = require('fs')
require('../mongoose')
const mongoose = require('mongoose')
const TypeModel = mongoose.model('types')
const yaml = require('js-yaml')
const _ = require('lodash')

exports.seedTypes = async function () {
  let collectionExists = await TypeModel.collection.length
  if (collectionExists) await TypeModel.collection.drop()

  try {
    console.time('typeSeeder')
    const types = await yaml.load(fs.readFileSync('./app/database/typeIDs.yaml', 'utf8'))

    for (var key in types) {
      if (!types.hasOwnProperty(key)) continue

      let type = types[key]
      type._id = parseInt(key)
      await TypeModel.collection.insert(type)
    }

    console.timeEnd('typeSeeder')

    return true
  } catch (e) {
    console.log(e)
  }

  return false
}

this.seedTypes().then(res => {
  if (!res) console.log('Something happened....')
  else console.log('Database Seeded')

  process.exit(0)
})
