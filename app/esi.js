const _ESI = require('eve-swagger').ESI
const ESI = new _ESI()
const axios = require('axios')
const _ = require('lodash')

const mongoose = require('mongoose')
const EsiTokensModel = mongoose.model('esi_tokens')
const CharacterModel = mongoose.model('character')
const CorporationModel = mongoose.model('corporation')
const AllianceModel = mongoose.model('alliance')
const ObserverModel = mongoose.model('observers')
const ObserverLedgerModel = mongoose.model('observer_ledgers')

module.exports.getAccessToken = async function (refreshToken) {
  const config = {
    headers: {
      Authorization: `Basic ${Buffer.from(`${process.env.ESI_CLIENT_ID}:${process.env.ESI_SECRET}`).toString('base64')}`,
      'Content-Type': 'application/json'
    }
  }

  const data = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken
  }

  const authData = await axios.post('https://login.eveonline.com/oauth/token', data, config)
    .catch(err => {
      console.error({ status: err.response.status, statusText: err.response.statusText })
      return null
    })

  if (authData.data && authData.data.access_token) {
    return authData.data.access_token
  }

  return null
}

module.exports.updateCharacterIfNotExists = async function (characterId) {
  let count = await CharacterModel.count({ _id: characterId })
  if (count <= 0) await this.updateCharacter(characterId)
}

module.exports.updateCharacter = async function (characterId) {
  characterId = parseInt(characterId)
  const character = await ESI.characters(characterId).details()
  character.portrait = await ESI.characters(characterId).portraits()

  return CharacterModel.findOneAndUpdate({ _id: characterId }, character, { upsert: true, new: true })
}

module.exports.updateCorporationIfNotExists = async function (corporationId) {
  let count = await CorporationModel.count({ _id: corporationId })
  if (count <= 0) await this.updateCorporation(corporationId)
}

module.exports.updateCorporation = async function (corporationId) {
  corporationId = parseInt(corporationId)
  const corporation = await ESI.corporations(corporationId).details()
  corporation.portrait = await ESI.corporations(corporationId).icons()

  return CorporationModel.findOneAndUpdate({ _id: corporationId }, corporation, { upsert: true, new: true })
}

module.exports.updateAllianceIfNotExists = async function (allianceId) {
  let count = await AllianceModel.count({ _id: allianceId })
  if (count <= 0) await this.updateAlliance(allianceId)
}

module.exports.updateAlliance = async function (allianceId) {
  allianceId = parseInt(allianceId)
  const alliance = await ESI.alliances(allianceId).details().catch(err => console.log(err))
  alliance.portrait = await ESI.alliances(allianceId).icons().catch(err => console.log(err))

  return AllianceModel.findOneAndUpdate({ _id: allianceId }, alliance, { upsert: true, new: true })
}

module.exports.updateObserverIfNotExists = async function (observerId, corporationId, accessToken) {
  let count = await ObserverModel.count({ _id: observerId })
  if (count <= 0) await this.updateObserver(observerId, corporationId, accessToken)
}

module.exports.updateObserver = async function (observerId, corporationId, accessToken) {
  const observerDetails = await ESI.corporations(corporationId, accessToken).structures(observerId).summary()
  observerDetails.corporation_id = corporationId

  return ObserverModel.findOneAndUpdate({ _id: observerId }, observerDetails, { upsert: true, new: true })
}

module.exports.updateLedger = async function (observerId, corporationId, accessToken) {
  const logs = await ESI.corporations(corporationId, accessToken).mining.observers(observerId).ledger()

  for await (let log of logs) {
    await this.updateCharacterIfNotExists(log.character_id)
    await this.updateCorporationIfNotExists(log.recorded_corporation_id)

    await ObserverLedgerModel.findOneAndUpdate({
      observer_id: observerId,
      observer_corporation_id: corporationId,
      last_updated: log.last_updated,
      character_id: log.character_id,
      type_id: log.type_id,
      recorded_corporation_id: log.recorded_corporation_id
    }, {quantity: log.quantity}, { upsert: true })
  }
}

module.exports.updateAllMiningLogs = async function () {
  const tokens = await EsiTokensModel.find({ roles: 'Director' })

  for (const token of tokens) {
    const accessToken = await this.getAccessToken(token.refresh_token)
    if (accessToken) {
      const observers = await ESI.corporations(token.corporation_id, accessToken).mining
        .observers().details()

      for await (const observer of observers) {
        /** Observer Updates should not be as frequent as Mining Logs */
        await this.updateObserverIfNotExists(observer[0], token.corporation_id, accessToken)

        /** Await so we can profile the update timespan */
        await this.updateLedger(observer[0], token.corporation_id, accessToken)
      }

      return {success: true}
    }
  }
}
