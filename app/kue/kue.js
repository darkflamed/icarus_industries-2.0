const mongoose = require('mongoose')
const EsiTokensModel = mongoose.model('esi_tokens')
const CharacterModel = mongoose.model('character')
const CorporationModel = mongoose.model('corporation')
const ObserverModel = mongoose.model('observers')
const ObserverLedgerModel = mongoose.model('observer_ledgers')
var kue = require('kue-scheduler')
var Queue = kue.createQueue()

var job = Queue
  .createJob('unique_every', data)
  .unique('unique_every')

Queue.every('60 minutes', job)

Queue.process('unique_every', function (job, done) {

  done()
})
